package ru.t1consulting.vmironova.tm.constant;

public class TerminalConst {

    public final static String VERSION = "version";

    public final static String ABOUT = "about";

    public final static String HELP = "help";

}
